exception Source_empty
let usage = "AVeRl erlang compiler"
let src = ref ""
let args = [
  ("-f", Arg.Set_string src, "source filename")
]
let parse_args = Arg.parse args print_endline usage

let prog_in src =
  let chan = Unix.open_process_in @@ "/home/ejhobbs/git/averl/res/script/erl_to_core " ^ src in
  Lexing.from_channel chan


let read_all _ = ()

let () =
  match !src with
  | "" -> Arg.usage args usage
  | s -> read_all @@ prog_in s
