(*
 * Lexer for core erlang binary output
 * The lack of error checking is due to the fact that we assume the erlang
 * compiler will not produce invalid output
 *)

let sign = '+'|'-'
let digit = ['0' - '9']
let name_char = (['a'-'z']|['A'-'Z']|'_')

let int = sign? digit+
let float = sign? digit+ '.' digit+ (('E'|'e') sign? digit+)?
let atom = name_char+

rule read =
  parse
  | eof   { EOF }
  | '{'   { L_BRACE }
  | '}'   { R_BRACE }
  | '['   { L_BRACK }
  | ']'   { R_BRACK }
  | ','   { COMMA }
  | '"'   { str   (Buffer.create 32) lexbuf }
  | '''   { name  (Buffer.create 32) lexbuf }
  | int   { INT   (int_of_string @@ Lexing.lexeme lexbuf) }
  | float { FLOAT (float_of_string @@ Lexing.lexeme lexbuf) }
  | atom  { ATOM  (Lexing.lexeme lexbuf) }
  | _ { read lexbuf } (* ignore anything we don't understand - includes whitespace *)

and str buf =
  parse
  | _   { Buffer.add_char buf (Lexing.lexeme lexbuf); str buf lexbuf}
  | '"' { STR (Buffer.contents buf) }

and name buf =
  parse
  | _   { Buffer.add_char buf (Lexing.lexeme lexbuf); name buf lexbuf }
  | ''' { NAME (Buffer.contents buf) }
