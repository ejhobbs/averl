-module(blinky_recursive).
-export([main/0]).

% assumptions
% - main will be repeated automatically (like arduino)
% - Rather than inferring types, which is a large job, instead we just run things through an "annotation" style system
%   where we explicitly state what type things will be. It looks a bit bad here since the methods were only one line,
%   but you can imagine if there was more meat to them then the relative increase in size would be far smaller.
% - setup method is run automatically (see void setup(void) in arduino. Run once so could actually do anything, but the
%   intention is for doing DDR setup etc.
% - Pins can now be accessed by arduino-style numbering (D0-A5). This is limiting since we miss a few, so there is also
%   still the old syntax-ish (avr:on(d,7)), allowing for full access to all the general I/O of the avr chip.
% - Recursion is replaced with a for loop using the parameter as the increment, and the first pattern as the stop condition

setup() ->
  avr:out(d6),
  avr:out(d7).

main() ->
  on(led1),
  off(led2),
  sleep(10), %% sleeps for 1000.. because recursion examples are difficult without lists
  off(led1),
  on(led2),
  timer:sleep(1000).

-spec sleep(integer()) -> none().
sleep(0) -> avr:int(0);
sleep(N) ->
  timer:sleep(100),
  sleep(N-1).


-spec on(atom()) -> none().
on(led1) -> avr:on(d6);
on(led2) -> avr:on(d7).

-spec off(atom()) -> none().
off(led1) -> avr:off(d6);
off(led2) -> avr:off(d7).
