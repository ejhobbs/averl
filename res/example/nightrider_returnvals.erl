-module(nightrider_returnvals).
-export([main/0]).

%% Simple "nightrider" style LED using bitshifts and global variables

setup() ->
  avr:make_global(int, dir),%% create an int in global scope called "dir"
  avr:out(d0),
  avr:out(d1),
  avr:out(d2),
  avr:out(d3),
  avr:out(d4),
  avr:out(d5),
  avr:out(d6),
  avr:out(d7),
  avr:on(d0). % light the first LED otherwise we'll get nothing

main() ->
  avr:set_global(dir, shift_leds(7)).

shift_leds(0) ->
  Dir = avr:read_global(dir),
  Dir bxor 1;

shift_leds(N) ->
  timer:sleep(50),
  Dir = avr:read_global(dir),
  Port = avr:read_reg(portd),
  case Dir of
    0 -> avr:set_reg(portd, Port bsl 1);
    1 -> avr:set_reg(portd, Port bsr 1)
  end,
  shift_leds(N-1).
