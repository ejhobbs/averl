-module(nightrider_recursive).
-export([main/0]).

%% Simple "nightrider" style LED using bitshifts and global variables

setup() ->
  avr:make_global(int, dir),%% create an int in global scope called "dir"
  avr:out(d0),
  avr:out(d1),
  avr:out(d2),
  avr:out(d3),
  avr:out(d4),
  avr:out(d5),
  avr:out(d6),
  avr:out(d7),
  avr:on(d0).

main() ->
  shift_leds(7).

shift_leds(0) ->
  Dir = avr:read_global(dir),
  case Dir of
    0 -> avr:set_global(dir, 1);
    1 -> avr:set_global(dir, 0)
  end;
shift_leds(N) ->
  Dir = avr:read_global(dir),
  Port = avr:read_reg(portd),
  case Dir of
    0 -> NewPort = Port bsl 1, avr:set_reg(portd, NewPort);
    1 -> NewPort = Port bsr 1, avr:set_reg(portd, NewPort)
  end,
  timer:sleep(50),
  shift_leds(N-1).
