default: build
build:
	dune build ./averl.exe
run:
	dune exec -- ./averl.exe -f $(FILE)
clean:
	dune clean
test:
	dune runtest
